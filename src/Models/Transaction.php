<?php

namespace Turahe\Royalty\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Turahe\Royalty\Models\Transaction.
 *
 * @property int $id
 * @property string $message
 * @property string $pointable_type
 * @property int $pointable_id
 * @property float $amount
 * @property float $current
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Model|\Eloquent $pointable
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereCurrent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction wherePointableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction wherePointableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Transaction extends Model
{
    /**
     * @var string
     */
    protected $table = 'transaction_points';

    protected $fillable = [
        'message', 'amount', 'current',
    ];

    public function pointable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @param Model $pointable
     *
     * @return static
     */
    public function getCurrentPoints(Model $pointable)
    {
        $currentPoint = self::
         where('pointable_id', $pointable->id)
         ->where('pointable_type', $pointable->getMorphClass())
         ->orderBy('created_at', 'desc')
         ->pluck('current')->first();

        if (! $currentPoint) {
            $currentPoint = 0.0;
        }

        return $currentPoint;
    }

    /**
     * @param Model $pointable
     * @param $amount
     * @param $message
     * @return static
     */
    public function addTransaction(Model $pointable, $amount, $message):self
    {
        $transaction = new static();
        $transaction->amount = $amount;

        $transaction->current = $this->getCurrentPoints($pointable) + $amount;

        $transaction->message = $message;

        $pointable->transactionPoints()->save($transaction);

        return $transaction;
    }
}
