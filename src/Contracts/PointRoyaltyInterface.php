<?php

namespace Turahe\Royalty\Contracts;

interface PointRoyaltyInterface
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function transactionPoints();

    // /**
    //  *
    //  * @return mix
    //  */
    public function averagePoint($round = null);

    //
    // /**
    //  *
    //  * @return mix
    //  */
    public function countPoint();

    //
    // /**
    //  *
    //  * @return mix
    //  */
    public function sumPoint();

    //
    // /**
    //  * @param $max
    //  *
    //  * @return mix
    //  */
    public function pointPercent($max = 5);

    /**
     * @return mix
     */
    public function countTransactions();

    /**
     * @param $amount
     * @param $message
     * @return static
     */
    public function addPoints($amount, $message);
}
