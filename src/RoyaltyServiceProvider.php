<?php

namespace Turahe\Royalty;

use Illuminate\Support\ServiceProvider;

class RoyaltyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $databasePath = __DIR__.'/../database/migrations';
        $this->loadMigrationsFrom($databasePath);
//        if ($this->isLumen()) {
//            $this->loadMigrationsFrom($databasePath);
//        } else {
//            $this->publishes([$databasePath => database_path('migrations')], 'migrations');
//        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
