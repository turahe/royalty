
# Laravel Pointable
Point Transaction system for laravel 5

## Installation

First, pull in the package through Composer.

```bash
composer require turahe/royalty
```

And then include the service provider within `app/config/app.php`.

```php
'providers' => [
    Turahe\Royalty\RoyaltyServiceProvider::class
];
```

At last you need to publish and run the migration.
```
php artisan vendor:publish --provider="Turahe\Royalty\PointableServiceProvider" && php artisan migrate
```

-----

### Setup a Model

```php
<?php

namespace App;

use Turahe\Royalty\Contracts\PointRoyaltyInterface;
use Turahe\Royalty\Traits\HasPointRoyalty as PointableTrait;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements PointRoyaltyInterface
{
    use PointableTrait;
}
```

### Add Points
```php
$user = User::first();
$amount = 10; // (Double) Can be a negative value
$message = "The reason for this transaction";

//Optional (if you modify the point_transaction table)
$data = [
    'ref_id' => 'someReferId',
];

$transaction = $user->addPoints($amount,$message,$data);

dd($transaction);
```

### Get Current Points
```php
$user = User::first();
$points = $user->currentPoints();

dd($points);
```

### Get Transactions
```php
$user = User::first();
$user->transactions;

//OR
//$user['transactions'] = $user->transactions(2)->get(); //Get last 2 transactions

dd($user);
```

### Count Transactions
```php
$user = User::first();
$user['transactions_total'] = $user->countTransactions();

dd($user);
```
